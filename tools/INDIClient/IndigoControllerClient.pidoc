\documentclass PIToolDoc

\tool IndigoDeviceController

\module IndigoDeviceController

\categories {
   Automation, Control
}

\keywords {
    Indigo, INDI, Device, Instrument, Automation, Control
}

\author {
   Klaus Kretzschmar
}

\copyright {
   2014-2021 Klaus Kretzschmar
}

\brief {
   Sends commands to an Indigo server to control astronomical devices.
}

\description {
   \image[size:50\%,float,marginRight:1.25em,marginBottom:0.5em] IndigoControllerClient.png

   The \e{Indigo Device Controller} is an Indigo client \ref indigo that can be used to control astronomical
   devices like CCD cameras, telescope mounts, or filter wheels locally and remotely over TCP/IP connections. The client sends 
   commands (Indigo Device Properties) to an Indigo server where these commands are translated to device specific
   control instructions \ref indigo_devices.

   The Indigo server is not part of the PixInsight installation and must be installed separately \ref indigo_download. The server can be operated
   locally on the same machine or remotely on a different machine in the local network. Indigo servers support Zeroconf/Bonjour 
   service discovery which allows clients to discover Indigo servers without manual configuration.  
   
   The Indigo protocol is compatible to INDI \ref indi_lib_overview, but provides extensions which go behind the INDI protocol according to  \ref indigo_extension. 
   Therefore, the \e{Indigo Device Controller} works with INDI as well as  Indigo servers, if you want to use the extensions 
   of the Indigo protocol you have to install the Indigo server. 
   
}

\usage {
 
   \definition {
     
      {Indigo Server Connection} {
        \image[marginRight:1.25em,marginBottom:0.5em] IndigoServerConnection.png 
        Specify the host and port number and connect to the Indigo or INDI server.
        \list [spaced]{
          {\s Hostname: The name of the host where the \e {Indigo server} processs is running on. Use \s localhost if
          \e PixInsight and \e{Indigo server} run on the same computer.}
          {\s Port: The port number where the \e {Indigo server} is listen to. The default port number of the \e {Indigo server} is \e 7624.}
          {\s Enable Indigo service detection: Check to search for Indigo servers in the local network.}
       }
       \image[marginRight:1.25em,marginBottom:0.5em] ServiceDetection.png

      After successful connection, the server sends a list of device names which are displayed in the \lref indi_devices {Indigo Devices} section.
   }
      {\label indi_devices Indigo Devices} {
      Select and connect to the Indigo devices which are available on the Indigo server.

      To connect a hardware device with the \e {Indigo server}, select the top level node in the \lref indi_devices {Indigo Devices}
      tree-box and press the \e {Connect} button.

      \image[marginRight:1.25em,marginBottom:0.5em] INDIDeviceSelection_1.png

      After successful connection, the server sends a list of device properties which are displayed in the \lref indi_devices
      {Indigo Devices} tree-box. Device properties are the control parameters of a hardware device, e.g. the property
      CCD_TEMPARATURE controls the chip temperature of a CCD camera.

      }

      {Edit and send new Indigo property values to the Indigo server} {

      All properties of a CCD device are listed as subnodes of the device in the  \lref indi_devices {Indigo Devices}
      tree-box. A property consists of one-or-many elements which
      are grouped in a property tree-node and its values can be displayed by expanding the node. There are three different types
      of property values

      \list {
          {\s Switch: A switch property can contain several elements which can only have two values: ON or OFF  }
          {\s Number: A number property can contain several elements which have a number as value.}
          {\s Text: A text property can contain everal elements which have a string as value.}
       }

      \image[marginRight:1.25em,marginBottom:0.5em] INDIPropertySelection.png

      It is possible to send a new target property value to the \e {Indigo server} by selecting the \e {property
      element} and pressing the \e Edit button. A new dialog appears which enables to a enter a new value for the property.

      \image[marginRight:1.25em,marginBottom:0.5em] INDIPropertyEdit.png
      }


      {\label server_message Server messages } {
        Messages from the {\e Indigo server} are displayed at the bottom of this section.
      \image[marginRight:1.25em,marginBottom:0.5em] INDIPropertyMessage.png
      }
   }
}

\relatedtools {
   IndigoCCDFrame, IndigoMount
}

\reference indigo {Indigo, \xref https://www.indigo-astronomy.org/ {INDIGO Astronomy}}

\reference indigo_download {Download Indigo, \xref https://www.indigo-astronomy.org/downloads.html {Download INDIGO}}

\reference indigo_extension {Indigo protocol, \xref https://github.com/indigo-astronomy/indigo/blob/master/indigo_docs/PROTOCOLS.md {INDIGO protocol}}

\reference indigo_devices {Indigo devices, \xref https://www.indigo-astronomy.org/for-users.html {INDIGO devices}}

\reference indi_lib_overview { INDI library, \xref http://indilib.org/about/discover-indi.html  { What is INDI? : An overview} }

\reference indi_protocol {INDI protocol, \xref http://www.clearskyinstitute.com/INDI/INDI.pdf  {INDI protocol specification}}




\make[noauthors]
