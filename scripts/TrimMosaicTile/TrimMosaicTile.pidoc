\documentclass PIScriptDoc

\script TrimMosaicTile

\keywords {
   mosaic
}

\author {
   John Murphy
}

\copyright {
   2019-2021 John Murphy
}

\brief {
    Erodes the edges of a registered mosaic tile back to good data.
}

\description {
  \subsection { Introduction } {
    This script is used to prepare registered (i.e. after MosaicByCoordinates) mosaic tiles for the PhotometricMosaic script. The PhotometricMosaic script requires that the images have hard edges. Their background level should remain at 100\% until the non zero image boundary, and then drop to 0\% at the boundary. TrimMosaicTile is used to errode the non zero area of an image to ensure hard edges.

    Soft edges can exist for several reasons. For example:
    \list {
      {When stacking images, some images might not contribute to an image edge. This can produce a staircase pattern at that edge.}
      {Whenever an image is registered (for example by MosaicByCoordinates), the image boundary will have soft antialiased edges. All images should therefore be trimmed back by at least 1 pixel, even if their edges look good.}
      {Area coverage may differ between color channels. The edges need to be eroded until all channels are present.}
    }

    The FITS header is updated with the amount trimmed from the image. PhotometricMosaic checks the FITS header for these entries, and writes a warning to the Console if either the reference or target images have not been trimmed.
  }
  \subsection {Quick Start Guide}{
    \image images/TrimMosaicTileAll.png

    \s{(1)} Select the \lref target_view {Target view} to be edited. Its window will be brought to the top of the window stack.

    \s{(2)} Select the \lref real_time_preview {Real time preview} checkbox. This creates a preview around the non zero area of the image, which will be used to display real time updates. See figure \fignum preview.

    \figure [numbered:preview]{
      \figtag \s {Preview created by TrimMosaicTile}
      \image images/M33_TrimPreview.png
      When the \lref real_time_preview {Real time preview} checkbox is selected, the script creates a preview around the non zero area of the image. This preview will be used to display updates as the Trim controls are updated.

      This is an image of the main view, after deselecting the \lref real_time_preview {Real time preview} checkbox. The green rectangle shows the size and position of the preview that was created for the \lref real_time_preview {Real time preview}.
    }

    \s{(3)} The \lref real_time_preview {Real time preview} initially shows the \lref image_outline {Image outline}. See figure \fignum real_time_preview, image (1).

    If you have applied any processing after registering the mosaic tiles, check that there are no gaps between this outline and the image edges. A gap indicates an 'almost black' region. Although these regions may look black, they must be removed before using PhotometricMosaic. To fix them, quit TrimMosaicTile and use PixelMath. For example, if the 'almost black' region has a maximum level of 0.0003, use either:
\n
    \s{iif($T < 0.0004, 0, $T)} or \s{$T - 0.0004}

    \figure [numbered:real_time_preview]{
      \figtag \s {Real time preview}
      \imageselect[menupos:bottom] {
        images/M33_ImageOutline.png { (1) Image outline}
        images/M33_trimmedImage.png { (2) Preview trimmed image}
        images/M33_undoRedo.png { (3) After Undo, the Preview displays the unedited image.}
      }
      \s{(1)} The \lref real_time_preview {Real time preview} with the \lref image_outline {Image outline} radio button selected. The non zero image outline is drawn in red (white for black & white images). In this example there are no gaps between the outline and the image.

      \s{(2)} The \lref real_time_preview {Real time preview} with the \lref preview_trimmed_image {Preview trimmed image} radio button selected. The preview shows the edit specified by the current \lref trim_controls {Trim controls}. The preview will automatically update whenever a control is adjusted. In this example, the preview has been zoomed to 2:1.

      \s{(3)} After pressing \lref undo {Undo}, the preview displays the unedited image. Use \lref undo {Undo} / \lref redo {Redo} to check the trim levels.
    }

    \s{(4)} Select the \lref preview_trimmed_image {Preview trimmed image} radio button. The edit specified by the \lref trim_controls {Trim controls} is applied to the preview. The preview will automatically update whenever a control is adjusted.

    In the example shown in figure \figref real_time_preview, image (3), we can see a 'staircase' reduction in background level on the left hand side of the image. On the top edge, we can see that a color channel does not extend to the top of the image. In image (2), 10 pixels have been trimmed from the top of the image, and 15 from the left. This removes both the 'staircase' and color problems. The right side and bottom edge (not visible in this example) looked good. However, it is always necessary to trim at least 1 pixel to remove the antialias softness created by the registration.

    Use the \lref scroll_x {Scroll X} and \lref scroll_y {Scroll Y} to inspect the whole perimeter. Use \lref undo {Undo} / \lref redo {Redo} to check the trim levels. Once satisfied, use the 'OK' button to apply the edit.
  }

  \subsection {Batch Processing}{
    If you carefully crop the stacked images back to good data before registering the mosaic tiles, it will only be necessary to errode the registered tiles by 1 pixel to remove the soft antialiased edges caused by registration.

    For a large mosaic of many tiles, the tiles can be edited as a batch by using the \s {ImageContainer} process. See figure \fignum image_container.

     \figure [numbered:image_container]{
      \figtag \s {Using TrimMosaicTile with an ImageContainer}
      \image images/imageContainer.png
    }

    \s{(1)} Invoke the TrimMosaicTile script. Set it to trim one pixel from all edges. Instead of executing it on a view, drag the blue triangle at the dialog's bottom left corner onto the PixInsight desktop to create a process icon.

    \s{(2)} Start the ImageContainer process (located at the bottom of the PROCESS menu, or use Ctrl+Alt+I). Specify the input files or views. If using files, fill in the 'Container Options' to specify where the trimmed images should be saved.

    \s{(3)} Drag the blue triangle at the bottom left corner of the ImageContainer onto the TrimMosaicTile process icon. The TrimMosaicTile script will then run in the background on all the specified images.
  }

}

\usage {

\image images/TrimMosaicTileAll.png

  \subsection { Target view \label target_view} {
    \image images/TargetView.png
    \definition {
      { Target view} {
        The selected view will be brought to the top of the window stack. This image will be trimmed by the amount specified by the \lref trim_controls {Trim controls} when the user selects the 'OK' button.
      }
      {Auto STF}{
        Applies a unlinked auto screen transfer function (STF) to the target view. The real time preview will use the target view's STF to visualise the data.
      }
    }
  }

  \subsection { Trim controls \label trim_controls} {
    \image images/TrimControls.png
    This group box contains the controls that determine the amount to erode from the top, bottom, left and right sides of the non zero area of the image.

    Registering the mosaic tiles to each other introduces a slightly soft antialiased edge, so at least one pixel should be trimmed from all sides even if the edges look good.
  }

  \subsection { Real time preview \label real_time_preview} {
    \image images/RealTimePreview.png
    When this group box is checked, a preview is created around the non zero area of the image.

    \definition {
      { Image outline \label image_outline } {
        When this radio button is selected, an outline is drawn around the unedited, non zero part of the image.

        If there are any gaps between this outline and the visible image, this indicates 'almost black' areas exist. These should be removed by quitting this script and then using PixelMath. For example, if the 'almost black' area peaks at 0.0002, the PixelMath expression would be either: \n
        \s{iif($T < 0.0003, 0, $T)} or \s{$T - 0.0003}
      }
      {Preview trimmed image \label preview_trimmed_image} {
        When this radio button is selected, the preview shows the effect of the current \lref trim_controls {Trim controls} on the image. The preview will update after each trim control adjustment.
      }
      {Scroll X \label scroll_x} {
        Scrolls the preview horizontally.
      }
      {Scroll Y \label scroll_y} {
        Scrolls the preview vertically.
      }
      {Zoom controls} {
        Preview zoom controls are provided to zoom in, zoom out or set the zoom to 1:1.
      }
      {Zoom to fit} {
        Zooms the preview to the largest size that will fit the PixInsight desktop, and adjusts the window size to fit the zoomed image. Use this button to provide an overview of the whole of the non zero image.
      }
      {1650 x 1050} {
        Resizes the window to 1650 x 1050 logical pixels. Since we are only interested in the edges of the non zero image, it is recommended that the TrimMosaicTile dialog is moved to the middle of the window.
      }
      {Undo / Redo \label undo \label redo} {
        Use 'Undo' to return to the unedited image. 'Redo' reapplies the edits.
      }
    }
  }

  \subsection { Check FITS headers for entries required by PhotometricMosaic } {
    \image images/EnterFitsHeaders.png
    If the FITS headers do not contain the entries required by PhotometricMosaic, a dialog will be displayed to allow the missing headers to be entered. This dialog is only displayed if one or more header entries are missing.

    The pixel size should be adjusted to take into account binning or a resized image. For example, with 2x binning or an image resized to half size, the specified pixel size should be 2x the sensor's pixel size.
  }

}

\relatedscripts {
  PhotometricMosaic, SplitMosaicTile, ImageSolver, ManualImageSolver, MosaicByCoordinates
}

\relatedtools {
  ScreenTransferFunction
}

\make
